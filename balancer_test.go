package balancer

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

var taskCount uint = 5
var maxErrors uint = 5

var mx sync.Mutex
var execCounter = 0

type testpair struct {
	function            []WorkerFunction
	expectedExecCounter int
	message             string
}

var tests = []testpair{
	{
		[]WorkerFunction{
			simpleWorker,
			simpleWorker,
			simpleWorker,
			simpleWorker,
			simpleWorker,
			simpleWorker,
		},
		6,
		"Test simple execution",
	},
	{
		[]WorkerFunction{
			simpleWorker,
			simpleWorker,
			ErrorWorker,
			ErrorWorker,
			ErrorWorker,
			ErrorWorker,
			ErrorWorker,
			ErrorWorker,
			ErrorWorker,
		},
		int(maxErrors) + 2,
		"Test max errors",
	},
}

func TestBalancer(t *testing.T) {
	for _, pair := range tests {
		execCounter = 0
		Balancer(pair.function, taskCount, maxErrors)
		assert.Equal(t, pair.expectedExecCounter, execCounter, pair.message)
	}
}

func simpleWorker() error {
	mx.Lock()
	defer mx.Unlock()
	execCounter += 1
	return nil
}
func ErrorWorker() error {
	mx.Lock()
	defer mx.Unlock()
	execCounter += 1
	return errors.New("error")
}
