package balancer

import "sync"

type WorkerFunction func() error

func Balancer(workers []WorkerFunction, maxWorkers uint, maxErrors uint) {
	var wg sync.WaitGroup
	wg.Add(len(workers))

	chanErrors := make(chan error, maxErrors)
	guard := make(chan struct{}, maxWorkers)

	for _, worker := range workers {
		guard <- struct{}{}
		go execWorker(worker, &wg, chanErrors, maxErrors)
		<-guard
	}
	wg.Wait()
}

func execWorker(worker WorkerFunction, wg *sync.WaitGroup, chanErrors chan<- error, maxErrors uint) {
	defer wg.Done()

	if len(chanErrors) < int(maxErrors) {
		workerError := worker()
		if workerError != nil {
			chanErrors <- workerError
		}
	}
}
